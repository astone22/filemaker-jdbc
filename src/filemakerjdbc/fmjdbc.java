package filemakerjdbc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.sql.*;
import java.io.*;

import com.filemaker.jdbc.Driver;

//
// fmjdbc by Alan Stone
//

public class fmjdbc {
	
	public static void main(String[ ] args)
	{
	// register the JDBC client driver
	try {
		Driver d = (Driver)Class.forName("com.filemaker.jdbc.Driver").newInstance();
	} catch(Exception e) {
		System.out.println(e);
	}
	// establish a connection to FileMaker
	Connection con = null;
	try {
		con = DriverManager.getConnection("jdbc:filemaker://localhost/FMServer_Sample?user=Admin&password=alan");
	} catch(Exception e) {
		System.out.println(e);
	}
	// get connection warnings
	SQLWarning warning = null;
	try {
		warning = con.getWarnings();
	if (warning == null) {
		// connection OK
		System.out.println("Connection successful: No warnings.");
		
		// create file to save data
		File file = new File("d:\\Alan\\output.txt");
		PrintWriter out = new PrintWriter(file.getAbsolutePath());
		
		
		
		
		// grab connection db metadata
		DatabaseMetaData dbmeta = con.getMetaData();

		
		System.out.println("TABLES");
		// grab the table names from db and put into arraylist
		ArrayList<String> tablelist = new ArrayList();
		ResultSet rs = dbmeta.getTables(null, null, "%", null);
	    while (rs.next()) {
	    	String tablename = rs.getString(3);
	    	tablelist.add(tablename);
	    	System.out.println(tablename);
	    }
	    
		
		/*
		System.out.println("TABLES");
		String tablelist[] = new String[];
		int tablecounter = 0;
		ResultSet rs = dbmeta.getTables(null, null, "%", null);
		while (rs.next()) {
			String tablename = rs.getString(3);
			//System.out.println(tablename);
			
			tablelist[tablecounter] = tablename;
			System.out.println(tablelist[tablecounter]);
			tablecounter++;
		}
	    */
		
		
	    System.out.println("COLUMNS");
	    ArrayList<String> columnlist = new ArrayList<String>();
	    // for each table name grab the column names
	    for (String i : tablelist){
	    	rs = dbmeta.getColumns(null, null, i, null);
	    	while (rs.next()){
	    		String columnname = rs.getString(4);
	    		columnlist.add(i + "," + columnname);
	    		System.out.println("TABLE: " + i + " // COLUMN: " + columnname);
	    	}
	    }
	    
	    
		/*
		System.out.println("COLUMNS");
		System.out.println(tablelist.length);
		for (int i = 0; i<tablelist.length; i++ ) {
			
		}
		*/
	    
	    
		
		String line = null;
		// grab data
		for (String i : columnlist){
			String[] split = i.split(",");
			System.out.println(split[0]);
			System.out.println(split[1]);
			String selectQuery = "SELECT \"" + split[1] + "\" FROM \"" + split[0] + "\"";
			PreparedStatement ps = con.prepareStatement(selectQuery);
			rs = ps.executeQuery();
			while (rs.next())
				line = "Table: " + split[0] + " // Column: + " + split[1] + " // Data for column: " + rs.getString(1);
				out.println(line);
		}
		out.close();
		
		
		
	}
	while (warning != null) {
		// print out warnings
		System.out.println("Warning: "+warning);
		warning = warning.getNextWarning();
	}
	} catch (Exception e) {
		System.out.println(e);
	}
	
	
	
	}
}
